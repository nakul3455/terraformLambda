import requests
import os
import json
import boto3
import base64

def lambda_handler(event, context):


  payload = event
  request_body = {
        "userId": 1,
        "id": 2,
        "title": "Lambda Function",
        "body": "Nakul "
    }

  #request_body = json.dumps(request_body_)
 
  request_headers = {
        "Content-Type": "application/json"
    }

  response = requests.post('https://jsonplaceholder.typicode.com/posts',
                             headers=request_headers,
                             data=json.dumps(request_body))

  base64_response = base64.b64encode(response.content).decode('utf-8')


  try:

      decoded_response = base64.b64decode(base64_response).decode('utf-8')

      print("Decoded response:", decoded_response)
  except Exception as e:
      print("Error decoding response:", str(e))
      decoded_response = None

  if response.status_code == 201:
    print(response.status_code)
    print("if")
    return {
      'statusCode': 201,
      'body': json.dumps({'message': 'Request sent successfully.'}),
    }
  else:
    print(response.status_code)
    print("else")
    return {
      'statusCode': response.status_code,
      'body': json.dumps({'message': 'The request failed.'}),
    }
